%==========================================================================
% MulT_predict.m
%
% FUNCTION:
% - Version 0.22: L. H. Ystroem (KIT) - March 2023
% MulT_predict is a MATLAB tool to facilitate the application of multicom-
% ponent geothermometry on geochemical data. Saturation indices of 
% mineral phases were automatically calculated via IPhreeqC and evaluated  
% by sensitivity analyses. The result is a best-fit temperature estimation.
% 
% INPUT:
% - xlsx-file:[Sample No., Name, Pressure, pH value, Temperature, Element
%             concentration]
% An xlsx-file, in a given template, can be read automatically and the geo-
% chemical data is passed to IPhreeqC.
%
% OUTPUT:
% - 'Name'.fig: [Saturation indices vs. temperature plot, statistics,
%               temperature estimation]
% A saved MATLAB figure, which contains a diagram of the saturation indi-
% ces of each utilized mineral phase plotted against the temperature, as 
% well as the statistics (Root mean square error, standard deviation, etc) 
% and the temperature estimation box plot of the best-fit values.
%
% - Text message: [steamloss/dilution [%], pH, and Al-concentration] 
% A text message in MATLABs Command Window with the details about the best 
% fit parameters (-steamloss/+dilution 'w'%, pH value is 'x', and Aluminium
% concentration 'y', and 'individual element' concentration is 'z'[unit]).
%
% WARNING:
% - Only one xlsx-file can be read and calculated at the time
% - The xlsx-file must be written in the given template
% - Mineral phases which do not intersect or multiply intersect with the 
%   equilibrium line is not taken into account.
%
% PLEASE NOTE:
% - MulT_predict was programmed in MATLAB R2020b;
% - To run MulT_predict, IPhreeqcCOM server (3.7.3) has to be installed
% - The instruction of MulT_predict is described in the following
%   '*'-framed section
%==========================================================================

%**************************************************************************
%* INSTRUCTION                                                            *
%* - To use MulT_predict, MATLAB and IPhreeqC must be installed           *
%* - Start MulT_predict via the Run-button in the Editor Window in MATLAB *
%* - Select an xlsx-file, in the recommended template, as an input file   *
%* - Select the samples which should be calculated                        *
%* - Select the temperature-, pH-, Al concentration- and dilution range   *
%* - Enter, if needed pH-, Al-, -steamloss/+dilution- fixation or leave   *
%*   it empty                                                             *
%* - Select mineral phases, which are taken into account for the tempera- *
%*   ture estimation                                                      *
%* - Runtime, status updates, and the best-fit parameters are printed in  *
%*   the Command Window in MATLAB                                         *
%* - The final results are given in a four-parted figure which is saved   *
%*   as a fig-file named like the selected well in the list               *
%*                                                                        *
%* - Changes in the used mineral phases or the size and numbers of        *
%*   sensitivity analysis steps must NOT be varied in the MATLAB Code!    *
%**************************************************************************

% Reading the xlsx-inputfile and listing all selectable samples
[filename,pathname] = uigetfile('*.xlsx', 'Select an Inputfile');
    if filename == 0
        fprintf('No Inputfile found \n')
        return
    else
        [numdata,header,raw] = xlsread([pathname,filename]);
        [rows,columns] = size(numdata);
        fprintf('Inputfile read \n')
        [cName,dName] = find(contains(header,'List')==1);
        b = size(header)- size(numdata);
        list1 = header((cName+b(1)):end,dName);
        listin = listdlg('ListString',list1);
    end

% Reading the values of the chosen sample (e.g. element concentration)
a = listin(1:end);
clear('SenMa','Sen1','Sen1m','Sen2','Sen2m')
ccon = find(contains(header(1,:),'['));
bcon = strfind(header(1,ccon),'[');
econ = strfind(header(1,ccon),']');
con = header{1,ccon}(1,cell2mat(bcon)+1:cell2mat(econ)-1);
Clist = {'Pressure','Temperature','pH','Al','Ba','CO2','Ca','Cl','Fe',...
    'H2S','K','Mg','Na','SiO2','SO4'};

for bc = 1:numel(Clist)
[~,db] = find(contains(header(1:2,:),Clist{1,bc})==1);
Clist{2,bc} = numdata(a,(db-b(2)));
clear('db')
end
struct = cell2struct(Clist(2,:),Clist(1,:),2);

% Inpute values for temperature, pH etc.
prompt = {'Enter minimum temperature [�C]:',...
'Enter maximum temperature [�C]:','Enter temperature steps:',...
'pH change per step +/-:','pH steps combined:',...
'Aluminium steps:','Boil/Dilu [%] per step +/-:',...
'Boil/Dilu steps combined:','pH fixation','Aluminium fixation',...
'-Boil/+Dilu fixation [%]','Threshold mineral phases'}; %,'Element steps:'
dlgtitle = 'Input';
definput = {'20','300','200','0.1','10','20','0','0','','','','6'};
dims = [1 32];
opts = 'off';
answer = inputdlg(prompt,dlgtitle,dims,definput,opts);
values = str2double(answer);
aaa = values(5)+1; % number of pH sensitivity steps
bbb = values(6)+1; % number of Al-con. sensitivity steps
ccc = values(8)+1; % number of steamloss/dilution sensitivity steps
ddd = 1;           % number of element-concentration sensitivity steps
                   % to activate change values(x)+1
if values(1) >= 1 && values(1) <= 250
tmin = values(1);
else; tmin = 20; % temperature lower limit
end
if values(2) >= 1 && values(2) <=351 % temperature upper limit
tmax = values(2);
else; tmax = 300;
end
tstp = values(3);
pHc = values(4);
cstp = values(7)/100;
if isempty(answer{11,1}) == 0 % fixed concentration
cfix = abs((values(11)/100)-1);
else; cfix = 1;
end
if isempty(answer{10,1}) == 0 % fixed Al-con.
struct.Al = values(10);
else; struct.Al = struct.Al;
end
if isempty(answer{9,1}) == 0 % fixed pH value
pHn = values(9);
else; pHn = struct.pH;
end
threshold = values(12); % threshold minerals

% Selection of possible mineral phases
%============================= llnl.dat ===================================
list2 = {'Calcite ','Araggonite ','Dolomite ','Spinel ','Hematite ',...
'Goethite ','Pyrite ','Pyrrhotite ','Diaspore ','Gibbsite ',...
'Anhydrite ','Gypsum ','Forsterite ','Grossular ','Andradite ',...
'Andalusite ','Sillimanite ','Kyanite ','Gehlenite ','Lawsonite ',...
'Epidote ','Zoisite ','Wollastonite ','Diopside ','Hedenbergite ',...
'Ferrosilite ','Enstatite ','Anthophyllite ','Tremolite ',...
'Pargasite ','Talc ','Muscovite ','Paragonite ','Phlogopite ',...
'Illite ','Smectite-high-Fe-Mg ','Smectite-low-Fe-Mg ',...
'Clinochlore-14A ','Clinochlore-7A ','Kaolinite ','Quartz ',...
'Chalcedony ','Anorthite ','Albite_high ','Albite_low ',...
'Sanidine_high ','K-Feldspar ','Maximum_Microcline ','Analcime ',...
'Laumontite ','Wairakite ','Barite ','Pyrophyllite ','Dolomite-dis ',...
'Dolomite-ord ','Beidellite-Ca ','Beidellite-K ','Beidellite-Mg ',...
'Beidellite-Na ','Montmor-Ca ','Montmor-K ','Montmor-Mg ','Montmor-Na ',...
'Annite ','Nepheline ','Nontronite-Ca ','Nontronite-H ',...
'Nontronite-K ','Nontronite-Mg ','Nontronite-Na ','Saponite-Ca ',...
'Saponite-H ','Saponite-K ','Saponite-Mg ','Saponite-Na ','Scolecite ',...
'Sepiolite ','Clinoptilolite-Ca ','Clinoptilolite-K ',...
'Clinoptilolite-Na ','Stilbite ','Natrolite ','Mordenite ','Mesolite ',...
'Chamosite-7A ','Ripidolite-14A ','Ripidolite-7A ','Margarite ',...
'SiO2(am) '};
[indx,tf] = listdlg('ListString',list2,'InitialValue',...
[1 11 32 35 40 41 45 47 48 49 76 81]);
%==========================================================================
minerals = list2(indx);
minC = strcat('si_',minerals(:));
minC = regexprep(minC, '\s', '');

% Defining the size of later on used matrices and vectors         
Sensitivity = cell(aaa*2,bbb,ccc,ddd); 
ValuepH = zeros(aaa,1);
ValueAl = zeros(bbb,1);
ValueC = zeros(ccc,1);
ValueK = zeros(ddd,1);
starttime = cputime;
fprintf('Sensitvity analysis started \n')

% For-loops of the several sensitivity analyses calculated via IPhreeqC
for bb = 1:bbb   % numbre of aluminium concentration sensitivity steps
for cc = 1:ccc   % number of steamloss/dilution sensitivity steps
for aa = 1:aaa   % number of pH sensitivity steps
for dd = 1:ddd   % number of individual concentration sensitivity steps
    iphreeqc = actxserver('IPhreeqcCOM.Object');
    iphreeqc.LoadDatabase(['C:\Program Files\USGS\IPhreeqcCOM' ...
        ' 3.7.3-15968\database\llnl.dat']); % pathname to IPhreeqcCOM
    iphreeqc.ClearAccumulatedLines;
    iphreeqc.AccumulateLine ('SOLUTION 1');
    iphreeqc.AccumulateLine (['-units ' con]);
    iphreeqc.AccumulateLine (['-pressure ' (num2str(struct.Pressure))]);
    iphreeqc.AccumulateLine (['-temperature ' (num2str(...
        struct.Temperature))]);
    iphreeqc.AccumulateLine (['-pH ' (num2str(struct.pH))]);

    if bb == 1  % size of aluminium concentration sensitivity steps
        iphreeqc.AccumulateLine (['Al ' (num2str(struct.Al))]);
        ValueAl(bb,:) = (struct.Al);
    elseif (bb > 1) && (bb <= bbb)
        iphreeqc.AccumulateLine (['Al ' (num2str((struct.Al)...
            *(0.1+1.9/(bbb-1)*(bb-1))))]);
        ValueAl(bb,:) = (struct.Al*(0.1+1.9/(bbb-1)*(bb-1)));
    end

    if cc == 1 % size of steamloss/dilution sensitivity steps or fix
        iphreeqc.AccumulateLine (['C(4) ' (num2str(struct.CO2/cfix))]);
        iphreeqc.AccumulateLine (['Si ' (num2str(struct.SiO2/cfix))]);
        iphreeqc.AccumulateLine (['Cl ' (num2str(struct.Cl/cfix))]);
        iphreeqc.AccumulateLine (['Ca ' (num2str(struct.Ca/cfix))]);
        iphreeqc.AccumulateLine (['S(-2) ' (num2str(struct.H2S/cfix))]);
        iphreeqc.AccumulateLine (['K ' (num2str(struct.K/cfix))]);
        iphreeqc.AccumulateLine (['Fe ' (num2str(struct.Fe/cfix))]);
        iphreeqc.AccumulateLine (['Na ' (num2str(struct.Na/cfix))]);
        iphreeqc.AccumulateLine (['Mg ' (num2str(struct.Mg/cfix))]);
        iphreeqc.AccumulateLine (['S(6) ' (num2str(struct.SO4/cfix))]);
        iphreeqc.AccumulateLine (['Ba ' (num2str(struct.Ba/cfix))]);
        ValueC(cc,:) = cfix;
    elseif (cc > 1) && (cc <= ((ccc-1)/2)+1)
        iphreeqc.AccumulateLine (['C(4) ' (num2str(struct.CO2/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Si ' (num2str(struct.SiO2/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Cl ' (num2str(struct.Cl/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Ca ' (num2str(struct.Ca/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['S(-2) ' (num2str(struct.H2S/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['K ' (num2str(struct.K/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Fe ' (num2str(struct.Fe/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Na ' (num2str(struct.Na/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Mg ' (num2str(struct.Mg/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['S(6) ' (num2str(struct.SO4/abs(cfix...
            +cstp*(cc-1))))]);
        iphreeqc.AccumulateLine (['Ba ' (num2str(struct.Ba/abs(cfix...
            +cstp*(cc-1))))]);
        ValueC(cc,:) = (cfix+cstp*(cc-1));
    elseif (cc > ((ccc-1)/2)+1) && (cc <= ccc)
        iphreeqc.AccumulateLine (['C(4) ' (num2str(struct.CO2/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Ca ' (num2str(struct.Ca/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Cl ' (num2str(struct.Cl/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Fe ' (num2str(struct.Fe/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['S(-2) ' (num2str(struct.H2S/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['K ' (num2str(struct.K/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Mg ' (num2str(struct.Mg/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Na ' (num2str(struct.Na/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Si ' (num2str(struct.SiO2/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['S(6) ' (num2str(struct.SO4/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        iphreeqc.AccumulateLine (['Ba ' (num2str(struct.Ba/abs(cfix...
            -cstp*(cc-((ccc-1)/2+1)))))]);
        ValueC(cc,:) = (cfix-cstp*(cc-((ccc-1)/2+1)));
    end
    
    if dd == 1  % size of individual concentration sensitivity steps
        iphreeqc.AccumulateLine (['Si ' (num2str(struct.SiO2))]);
        ValueK(dd,:) = (struct.SiO2);
    elseif (dd > 1) && (dd <= ddd)
        iphreeqc.AccumulateLine (['Si ' (num2str((struct.SiO2)*(0.8+0.4/...
            (ddd-1)*(dd-1))))]);
        ValueK(dd,:) = (struct.SiO2*(0.8+0.4/(ddd-1)*(dd-1)));
    end
    
    if aa == 1  % size of pH sensitivity steps
        if struct.pH <= pHn
        iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
        iphreeqc.AccumulateLine (['Fix_pH -' (num2str(pHn)) ' NH3']);
        else
        iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
        iphreeqc.AccumulateLine (['Fix_pH -' (num2str(pHn)) ' HNO3']);
        end
        ValuepH(aa,:) = (pHn);
    elseif (aa > 1) && (aa <= ((aaa-1)/2)+1)
        if struct.pH < pHn-pHc*(aa-1)
        iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
        iphreeqc.AccumulateLine (['Fix_pH -' (num2str(pHn-pHc...
            *(aa-1))) ' NH3']);
        else
        iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
        iphreeqc.AccumulateLine (['Fix_pH -' (num2str(pHn-pHc...
            *(aa-1))) ' HNO3']);
        end
        ValuepH(aa,:) = (pHn-pHc*(aa-1));
    elseif (aa > ((aaa-1)/2)+1) && (aa <= aaa)
        if struct.pH <= pHn+pHc*(aa-(((aaa-1)/2)+1))
        iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
        iphreeqc.AccumulateLine (['Fix_pH -' (num2str(pHn+pHc...
            *(aa-(((aaa-1)/2)+1)))) ' NH3']);
        else
        iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
        iphreeqc.AccumulateLine (['Fix_pH -' (num2str(pHn+pHc...
            *(aa-(((aaa-1)/2)+1)))) ' HNO3']);
        end
        ValuepH(aa,:) = (pHn+pHc*(aa-(((aaa-1)/2)+1)));
    end

% pH fixation in IPhreeqC
    iphreeqc.AccumulateLine ('PHASES');
    iphreeqc.AccumulateLine ('Fix_pH');
    iphreeqc.AccumulateLine ('H+ = H+');
    iphreeqc.AccumulateLine ('-log_k 0');
    iphreeqc.AccumulateLine ('REACTION_TEMPERATURE 1');
    iphreeqc.AccumulateLine ([(num2str(tmin)) ' ' (num2str(tmax))...
        ' in ' (num2str(tstp)) ' steps']);
    iphreeqc.AccumulateLine ('SELECTED_OUTPUT 1');
    iphreeqc.AccumulateLine (['-si ' minerals{:}]);
    iphreeqc.AccumulateLine ('-temperature');
    iphreeqc.AccumulateLine ('-pH');
    iphreeqc.AccumulateLine ('-totals Al');
    try
    iphreeqc.RunAccumulated;
        
% Extraction of temperature data and saturation indices of mineral phases
        out_PHREEQC = iphreeqc.GetSelectedOutputArray;
        names = out_PHREEQC(1,:);
        Te = find(strncmp('temp',names,4));
        te = 2:size(out_PHREEQC);
        Temperature = cell2mat(out_PHREEQC(te,Te));
        Si = find(strncmp('si_',names,3));
        Matrix = cell2mat(out_PHREEQC(te,Si)); 
        Matrix(Matrix == 0) = 0.001;
        
% Clean up of non- or multiple-intersecting mineral phases
        Data = diff(sign(Matrix));
        d = any(Data(2:end,:));
        e = sum(Data(2:end,(d == 1)));
        f = find(e == 0);
        Matrix(:,(d == 0)) = [];
        Matrix(:,f) = [];
        NameLegend = names(Si(1):end);
        NameLegend(d == 0) = [];
        NameLegend(f) = [];
        Data = diff(sign(Matrix));
        g = sum(abs(Data(2:end,:)));
        Matrix(:,(g > 2)) = [];
        NameLegend(g > 2) = [];
        
% Calculation of equilibrium temperature of each mineral phase        
            Temp = zeros(1,numel(NameLegend));
            h = 1;
            for j = 1:numel(NameLegend)
                Temp(h) = Temperature(find(diff(sign(Matrix(2:end,j)))));
                h = h+1;
            end
            
% Saving equilibrium data (temperatures of mineral phases) in a cell-array        
        Sensitivity{aa*2-1,bb,cc,dd} = NameLegend;
        Sensitivity{aa*2,bb,cc,dd} = num2cell(Temp);
        if numel(cell2mat(Sensitivity{aa*2,bb,cc,dd})) <= threshold
            Sensitivity{aa*2-1,bb,cc,dd} = minC(:).';
            Sensitivity{aa*2,bb,cc,dd} = {nan(1)};
        end        
        catch
           
    end
end
end
end
Time = cputime - starttime
end
fprintf('Sensitvity analysis completed \n')

% Clean up mineral phases for a consistent, statistically evaluable dataset 
for rr = 1:size(Sensitivity,2)
   for tt = 1:2:size(Sensitivity,1)
      for oo = 1:size(Sensitivity,3)
         for uu = 1:size(Sensitivity,4)
            try
               x = setdiff(Sensitivity{1,1,1,1},Sensitivity{tt,rr,oo,uu});
               z = setdiff(Sensitivity{tt,rr,oo,uu},Sensitivity{1,1,1,1});
                    if  isempty(x) == 0
                      for ee = 1:size(Sensitivity,2)
                         for ff = 1:2:size(Sensitivity,1)
                            for pp = 1:size(Sensitivity,3)
                               for vv = 1:size(Sensitivity,4)
                                  y = find(contains(Sensitivity...
                                      {ff,ee,pp,vv},x));
                                  for gg = length(y):-1:1
                                    Sensitivity{ff,ee,pp,vv}...
                                        (:,y(1,gg)) = [];
                                    Sensitivity{ff+1,ee,pp,vv}...
                                        (:,y(1,gg)) = [];
                                  end
                               end
                            end
                         end    
                      end
                    elseif isempty(z) == 0
                      for ee = 1:size(Sensitivity,2)
                         for ff = 1:2:size(Sensitivity,1)
                            for pp = 1:size(Sensitivity,3)
                               for vv = 1:size(Sensitivity,4)
                                  y = find(contains(Sensitivity...
                                      {ff,ee,pp,vv},z));
                                  for gg = length(y):-1:1
                                    Sensitivity{ff,ee,pp,vv}...
                                        (:,y(1,gg)) = [];
                                    Sensitivity{ff+1,ee,pp,vv}...
                                        (:,y(1,gg)) = [];
                                  end
                               end
                            end 
                         end
                      end
                    end
            catch
            end
         end
      end
   end
end
fprintf('Mineral comparison completed \n')

% Statistical evaluation of the matched data and extraction in a matrix
SenMa = zeros(size(Sensitivity,1)/2,size(Sensitivity,2),...
    size(Sensitivity,3),size(Sensitivity,4));
for ll = 1:size(Sensitivity,2)
    for ii = 2:2:size(Sensitivity,1)
        for qq = 1:size(Sensitivity,3)
            for ww = 1:size(Sensitivity,4)
                if isempty(Sensitivity{ii,ll,qq,ww}) == 1
                    Sensitivity{ii,ll,qq,ww} = {nan(1)};
                end
                SenMa(ii/2,ll,qq,ww) = range(cell2mat(Sensitivity...
                    {ii,ll,qq,ww}));
            end
        end
    end
end
[Mmin,Imin] = min(SenMa(:),[],'omitnan');
[Ib_row, Ib_col, Ib_pag, Ib_cub] = ind2sub(size(SenMa),Imin);
NameLegende = Sensitivity{2*Ib_row-1,Ib_col,Ib_pag,Ib_cub};
NameLegende = regexprep(NameLegende, 'si_', ' ');
fprintf('Statistics completed \n')

% Rerun IPhreeqC with the best fit values obtained by sensitivity analyses
% Enter ValueK(Ib_cub) for individual element concentration
iphreeqc = actxserver('IPhreeqcCOM.Object');
iphreeqc.LoadDatabase(['C:\Program Files\USGS\' ...
    'IPhreeqcCOM 3.7.3-15968\database\llnl.dat']);% pathname to IPhreeqcCOM
iphreeqc.ClearAccumulatedLines;
iphreeqc.AccumulateLine ('SOLUTION 1');
iphreeqc.AccumulateLine (['-units ' con]);
iphreeqc.AccumulateLine (['-pressure ' (num2str(struct.Pressure))]);
iphreeqc.AccumulateLine (['-temperature ' (num2str(struct.Temperature))]);
iphreeqc.AccumulateLine (['Al ' (num2str(ValueAl(Ib_col)))]);
iphreeqc.AccumulateLine (['C(4) ' (num2str(struct.CO2/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['Ca ' (num2str(struct.Ca/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['Cl ' (num2str(struct.Cl/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['Fe ' (num2str(struct.Fe/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['S(-2) ' (num2str(struct.H2S/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['K ' (num2str(struct.K/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['Mg ' (num2str(struct.Mg/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['Na ' (num2str(struct.Na/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine (['Si ' (num2str(struct.SiO2/ValueC(Ib_pag)))]); 
iphreeqc.AccumulateLine (['Ba ' (num2str(struct.Ba/ValueC(Ib_pag)))]);
iphreeqc.AccumulateLine ('EQUILIBRIUM_PHASES 1');
    if struct.pH <= ValuepH(Ib_row)
    iphreeqc.AccumulateLine (['Fix_pH -' (num2str(ValuepH(Ib_row)))...
        ' NH3']);
    else
    iphreeqc.AccumulateLine (['Fix_pH -' (num2str(ValuepH(Ib_row)))...
        ' HNO3']);
    end
iphreeqc.AccumulateLine ('PHASES');
iphreeqc.AccumulateLine ('Fix_pH');
iphreeqc.AccumulateLine ('H+ = H+');
iphreeqc.AccumulateLine ('-log_k 0');
iphreeqc.AccumulateLine ('REACTION_TEMPERATURE 1');
iphreeqc.AccumulateLine ([(num2str(tmin)) ' ' (num2str(tmax))...
    ' in ' (num2str(tstp)) ' steps']);
iphreeqc.AccumulateLine ('SELECTED_OUTPUT 1');
iphreeqc.AccumulateLine (['-si' NameLegende{:}]);
iphreeqc.AccumulateLine ('-temperature');
iphreeqc.AccumulateLine ('-pH');
iphreeqc.RunAccumulated;

% Extraction of temperature data and saturation indices of mineral phases
out_PHREEQCB = iphreeqc.GetSelectedOutputArray;
namesB = out_PHREEQCB(1,:);
TeB = find(strncmp('temp',namesB,4));
teB = 2:size(out_PHREEQCB);
Temperature = cell2mat(out_PHREEQCB(teB,TeB));
SiB = find(strncmp('si_',namesB,3)); 
MatrixB = cell2mat(out_PHREEQCB(teB,SiB));
MatrixB(MatrixB == 0) = 0.001;

% Clean up of non- or multiple-intersecting mineral phases
DataB = diff(sign(MatrixB));
e = any(DataB(2:end,:));
f = sum(DataB(2:end,(e == 1)));
g = find(f == 0);
MatrixB(:,(e == 0)) = [];
MatrixB(:,g) = [];
NameLegendB = namesB(SiB(1):end);
NameLegendB(e == 0) = [];
NameLegendB(g) = [];
DataB = diff(sign(MatrixB));
i = sum(abs(DataB(2:end,:)));
MatrixB(:,(i > 2)) =[];
NameLegendB(i > 2) = [];

% Calculation of equilibrium temperature of each mineral phase
        Best = zeros(1,numel(NameLegendB));
        jj = 1;
        for kk = 1:numel(NameLegendB)
            Best(jj) = Temperature(find(diff(sign(MatrixB(2:end,kk)))));
            jj = jj+1;
        end

% Visulisation of saturation curves
o = 2:size(Temperature);
figure
u = header(a+b(1),dName);
sgtitle(u);
plotbrowser('on')
plot(Temperature(o),MatrixB(o,:));
xlabel('Temperature [�C]')
ylabel('Saturation index')
line([0 max(Temperature)], [0 0],'Linestyle','--','Color','k')
h = legend(NameLegendB,'Location','best');
h.Interpreter = 'none';
keyboard % possibility to delete false phases or just click on "RUN" again 
fc = get(gca,'Children');
hLegend = findobj(gcf,'Type','Legend');
LegendB = hLegend.String;
MatrixB = get(fc,'YData');
MatrixB = (flip(cell2mat(MatrixB(2:end,:))))';
Best = zeros(1,numel(LegendB));
ss = 1;
    for tt = 1:numel(LegendB)
        Best(ss) = Temperature(find(diff(sign(MatrixB(2:end,tt)))));
        ss = ss+1;
    end

% Statistics and output of the best fit parameters        
RMSE = rms(MatrixB');
SDEV = std(MatrixB');
RMED = median(abs(MatrixB'));
MEAN = mean(abs(MatrixB'));
fprintf('Best fit completed \n')
Results = [(1-ValueC(Ib_pag))*100, ValuepH(Ib_row),ValueAl(Ib_col),...
    ValueK(Ib_cub)];
formatSpec = ['-Steamloss/+Dilution %2.0f%%, pH is %4.3f, Aluminium' ...
    ' concentration %5.6f, and SiO2 concentration %.3f %s \n'];
fprintf(formatSpec,Results,con)

% Ploting and saving of the best fit data
LegendB = regexprep(LegendB, 'si_', '');
o = 2:size(MatrixB);
figure
u = header(a+b(1),dName);
sgtitle(u);
subplot(2,2,1) %[1,2]
plot(Temperature(o),MatrixB(o,:));
title('Saturation indices')
xlabel('Temperature [�C]')
ylabel('Saturation index')
line([0 max(Temperature)], [0 0],'Linestyle','--','Color','k')
h = legend(LegendB,'Location','best');
h.Interpreter = 'none';
legend('boxoff');

subplot(2,2,3)
t = plot(Temperature(o),RMSE(o),Temperature(o),SDEV(o),Temperature(o),...
         RMED(o),Temperature(o),MEAN(o));
title('Statistics')
xlabel('Temperature [�C]')
ylabel('Saturation index')
legend('RMES','SDEV','RMED','MEAN');

subplot(2,2,4)
boxplot(Best,'whisker',8);
ylim([0 max(Temperature)])
title('Temperature estimation')
xticklabels(strjoin(u))
ylabel('Temperature [�C]')
drawnow;

subplot(2,2,2)
for xx = 1:numel(ValueC)
surf(sort(ValueAl),ValuepH,SenMa(:,:,xx),'FaceColor','interp'); hold on
end
xlabel('Al concentration')
ylabel('pH value')
zlabel('\Delta T')

saveas(gcf, strjoin(u),'fig');
fprintf('Sample finished \n')
% Have fun, good luck, and happy research!