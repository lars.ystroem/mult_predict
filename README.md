# MulT_predict - An optimised comprehensive multicomponent geothermometer

## Name
MulT_predict

## Description
A fully integrated solute multicomponent geothermometer, combining numerical optimisation processes for sensitive parameters (pH, Al-concentration, steam loss/dilution) to back-calculate to the chemical reservoir
conditions for precise reservoir temperature prediction.

## Function
MulT_predict couples MATLAB to an IPhreeqcCOM server determining the equilibrium state of a chosen mineral set to serve as a multicomponent geothermometer. The integrated optimisation process calculates a multi-dimensional cell array comprising all results of the interdependent variations of the sensitive parameters. The global minimum within this data represents the maximal convergence of the
equilibrium temperatures of the considered mineral phases. Thus, determining the position of the global minima in the array, results in the back-calculated in-situ values of the reservoir parameters. 

## Installation
MulT_predict is based on MATLAB (R2022a), and IPhreeqc (3.7.3-15968).

## Usage
INPUT: xlsx-file: [Sample No., Name, Pressure, pH value, Temperature, Element concentration]

OUTPUT: 'name'.fig: [Saturation indices vs. Temperature plot, Statistics, Temperature estimation]

A MATLAB figure, which contains a diagram of the saturation indices of each utilised mineral phase plotted against the temperature, as well as the statistics (Root Mean Square, Standard Deviation, etc.) and the temperature estimation box plot of the best-fit values.

## Support
lars.ystroem@kit.edu

## Author
Ystroem, L.H.

## License
MIT License
